import {Hero} from '../app/hero';

export const Heroes: Hero[]=[
   {id:12,heroName:'Captain America'},
    {id:13,heroName:'Thor'},
    {id:14,heroName:'Iron Man'},
    {id:15,heroName:'Winter Soldier'},
    {id:16,heroName:'Black Panther'},
    {id:17,heroName:'Doctor Strange'},
    {id:18,heroName:'Hulk'}
];